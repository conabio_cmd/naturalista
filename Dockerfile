# Start with CUDA base image
FROM ezentenoj/caffe
MAINTAINER Daniel Zenteno <daniel.zenteno@conabio.gob.mx>

COPY ./python/naturalistas/requirements.txt /tmp/
RUN pip install -r /tmp/requirements.txt

#Install jupyter
# RUN pip install -U jupyter
# RUN jupyter notebook --generate-config
# COPY ./jupyter_notebook_config.py /root/.jupyter/jupyter_notebook_config.py 
