# Naturalista using Googlenet #

Mining naturalista to predict 21 species of birds in Mexico City. The model used is described in [https://github.com/BVLC/caffe/tree/master/models/bvlc_googlenet](https://github.com/BVLC/caffe/tree/master/models/bvlc_googlenet)

## Create dataset ##

Class **python/naturalistas.api.BirdsCDMX.py**, method **download_dataset** contains an example to extract images from Naturalista.

Once all the images from Naturalista are downloaded. You may want to add more extra data using Flickr or other service. We suggest you to have at least 4000 images per taxon. If you can't download these many images, try applying noise and transformations to your originals.

Once you have all your images, you can use method **resize_dataset** to have all your images compatible with Googlenet.

Finally, use method **dataset_to_txt_file** to have the necessary files to run [convert_imaget](https://github.com/BVLC/caffe/blob/master/tools/convert_imageset.cpp) from Caffe to create train_lmdb and test_lmdb

## Training ##

Use **model/train_val.prototxt** and **model/quick_solver.prototxt** to train the net. Change the last InnerProduct layer to match the number of output and the number of total of classes. For instance, if you want to classify 100 classes, the last InnerProduct layer has to have 100 outputs.

## Testing ##

A ready to run web-app is included within this repo. To run the service, see **docker-compose.yml**

## Running on Amazon Linux AMI ##

### Install docker ###

[http://docs.aws.amazon.com/AmazonECS/latest/developerguide/docker-basics.html#install_docker](Link URL)

### Install docker-compose ###

$ curl -L https://github.com/docker/compose/releases/download/1.6.2/docker-compose-`uname -s`-`uname -m` > ~/bin/docker-compose

$ chmod +x ~/bin/docker-compose

### Start service ###
$ docker-compose up